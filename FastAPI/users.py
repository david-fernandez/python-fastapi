from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

# Entidad User
class User(BaseModel):
    id: int
    name: str
    surname: str
    url: str
    age: int

users_list = [
            User(id = 1, name = "David", surname = "Muñoz Fernandez", url = "https://google.com", age = 33),
            User(id = 2, name = "Pepe", surname = "Perez Fernandez", url = "https://google.com", age = 31),
            User(id = 3, name = "Juan", surname = "Gomez Fernandez", url = "https://google.com", age = 35),
        ]

@app.get("/usersjson")
async def usersjson():
    return [
        {"name": "David", "surname": "Muñoz Fernandez", "url": "https://google.com", "age": 33},
        {"name": "Pepe", "surname": "Perez Calvo", "url": "https://google.com", "age": 1},
        {"name": "Valentín", "surname": "Rosendo García", "url": "https://google.com", "age": 33},
    ]

@app.get("/users")
async def users():
    return users_list

# Path
# ----
@app.get("/user/{id}")
async def user(id: int):
    search_user(id)

# Querys
# ------
@app.get("/user/")
async def user(id: int):
    search_user(id)
    

@app.post("/user/")
async def user(user: User):
    if type(search_user(user.id)) == User:
        return {"error": "El usuario ya existe"}
    else:
        users_list.append(user)


def search_user(id: int):
    users = filter(lambda user: user.id == id, users_list)
    try:
        return list(users)[0]
    except:
        return {"error": "No se han econtrado el usuario"}